﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    public class Insertion : Sort
    {
        public Insertion(int[] q)
        {
            this.query = q;
        }


        public Insertion(int queryLength, int range)
        {
            this.SetRandomQuery(queryLength, range);
        }
        override public void Sorting()
        {
            int i, j, tmp;
            for (i = 1; i < this.query.Length; i++)
            {
                tmp = this.query[i];//正在處理的數字
                for (j = i; j > 0 && tmp < this.query[j - 1]; j--)//由已排序好的最後開始往前找
                    this.query[j] = this.query[j - 1];//每個數字往後移動一位 除非遇到正確位置 則跳出迴圈
                this.query[j] = tmp;//正在處理的數字 放入 遇到較大的數字位置 
            }
        }
    }
}
