﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    public class Selection : Sort
    {

        public Selection(int[] q)
        {
            this.query = q;
        }


        public Selection(int queryLength, int range)
        {
            this.SetRandomQuery(queryLength, range);
        }

        override public void Sorting()
        {
            for (int i = 0; i < this.query.Length; i++)
            {
                int minIndex = i;//已排序好的最後一個位置
                for (int j = i + 1; j < this.query.Length; j++)
                {
                    if (this.query[j] < this.query[minIndex])
                        minIndex = j;
                }
                if (minIndex != i)//一樣的交換沒意義
                    swap(this.query, i, minIndex);
                

            }
        }
    }
}
