﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    public class Quick : Sort
    {
        private int count { get; set; }
        public Quick(int[] q)
        {
            this.count = 0;
            this.query = q;
        }

        public Quick(int queryLength, int range)
        {
            this.SetRandomQuery(queryLength, range);
        }

        public override void Sorting()
        {
            QuickSort(0, this.query.Length - 1);
        }

        private void QuickSort(int left, int right)
        {
            if (left < right)
            {
                int s = this.query[(left + right) / 2];//中間切一半
                int i = left - 1;  //修正while 先+1的問題
                int j = right + 1; //修正while 先+1的問題
                while (true)
                {
                    while (this.query[++i] < s) ;  // 向右找
                    while (this.query[--j] > s) ;  // 向左找

                    if (i >= j) //全部交換正確位置
                        break;
                    swap(this.query, i, j);
                }
                QuickSort(left, i - 1);   // 對左邊進行遞迴
                QuickSort(j + 1, right);  // 對右邊進行遞迴
            }
        }


    }
}
