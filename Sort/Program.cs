﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    class Program
    {
        static void Main(string[] args)
        {

            List<ISort> sorts = new List<ISort>();
            int[] query = { 93, 34, 63, 85, 3, 44, 70 };

            #region BubbleSort
            //BubbleSort
            /*
            Best Case：Ο(n)
                當資料的順序恰好為由小到大時
                第一次執行後，未進行任何swap ⇒ 提前結束
            Worst Case：Ο(n2)
                當資料的順序恰好為由大到小時
                每回合分別執行：n-1、n-2、...、1次
                (n-1) + (n-2) + ... + 1 = n(n-1)/2 ⇒ Ο(n2)
            Average Case：Ο(n2)
                第n筆資料，平均比較(n-1)/2次
            */

            sorts.Add(new Bubble(query));
            #endregion

            #region SelectSort

            /*SelectSort
             時間複製度：最差與平均時間O(n2)
             從未排序的數列中找到最小的元素。
             將此元素與已排序部分的尾端元素進行交換。
             重複以上動作直到未排序數列全部處理完成。
             */

            query = new int[] { 93, 34, 63, 85, 3, 44, 70 };
            sorts.Add(new Selection(query));

            #endregion

            #region Insertion
            query = new int[] { 93, 34, 63, 85, 3, 44, 70 };
            sorts.Add(new Insertion(query));

            #endregion

            #region QuickSort

            query = new[] { 93, 34, 63, 85, 3, 44, 70 };
            sorts.Add(new Quick(query));

            #endregion

            #region Heap

            query = new[] { 93, 34, 63, 85, 3, 44, 70 };
            sorts.Add(new Heap(query));

            #endregion

            #region Marge

            query = new[] { 93, 34, 63, 85, 3, 44, 70 };
            sorts.Add(new Marge(query));

            #endregion

            #region Shell

            query = new[] { 93, 34, 63, 85, 3, 44, 70 };
            sorts.Add(new Shell(query));

            #endregion

            #region Radix

            query = new[] { 93, 34, 63, 85, 3, 44, 70 };
            sorts.Add(new Radix(query));

            #endregion

            #region ShowResult

            foreach (Sort sort in sorts)
            {
                sort.Show();
            }

            #endregion




            Console.ReadLine();
        }
    }
}
