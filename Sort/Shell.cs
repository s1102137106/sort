﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    public class Shell : Sort
    {
        public Shell(int[] q)
        {
            this.query = q;
        }

        public override void Sorting()
        {
            ShellSort(this.query);
        }

        private void ShellSort(int[] array)
        {
            int gap = array.Length / 2;
            int temp;

            while (gap > 0)
            {
                for (int i = 0; i + gap < array.Length; i++)
                {
                    temp = array[i + gap];
                    int j;
                    for (j = i + gap; j - gap >= 0 && temp < array[j - gap]; j = j - gap)
                    {
                        array[j] = array[j - gap];
                    }
                    array[j] = temp;
                }

                gap = gap / 2;
            }
        }

    }
}
