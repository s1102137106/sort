﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    public class Bubble : Sort
    {

        public Bubble(int[] q)
        {
            this.query = q;
        }

        public Bubble(int queryLength, int range)
        {
            this.SetRandomQuery(queryLength, range);
        }

        override public void Sorting()
        {
            for (int i = 0; i < this.query.Length; i++)
            {
                //this.query.Length-i-1 代表 每過一次外層迴圈會固定一個數字
                bool change = false;
                for (int j = 0; j < this.query.Length - i - 1; j++)
                {
                    if (this.query[j] > this.query[j + 1])
                    {
                        change = true;
                        swap(this.query, j, j + 1);
                    }
                }

                //如果都沒有交換 直接跳離迴圈
                if (!change)
                    break;
            }
        }


    }
}
