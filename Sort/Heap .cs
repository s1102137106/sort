﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    class Heap : Sort
    {
        public Heap(int[] q)
        {
            this.query = q;
        }

        public override void Sorting()
        {
            HeapSort(this.query);
        }

        private void HeapSort(int[] input)
        {
            //Build-Max-Heap
            for (int p = (input.Length - 1) / 2; p >= 0; p--)
                MaxHeapify(input, input.Length, p);

            //排序 從最後一筆開始 跟第一筆資料交換後 做Heapify
            for (int i = input.Length - 1; i > 0; i--)
            {
                swap(input, i, 0);
                MaxHeapify(input, i, 0);
            }
        }

        /// <summary>
        /// 堆積化
        /// </summary>
        /// <param name="input">要排序的數列</param>
        /// <param name="heapSize"></param>
        /// <param name="index"></param>
        private void MaxHeapify(int[] input, int heapSize, int index)
        {
            int left = (index + 1) * 2 - 1;//以此INDEX的左子樹
            int right = (index + 1) * 2;//以此INDEX的右子樹
            int largest = index;//預設中間最大

            if (left < heapSize && input[left] > input[index])
                largest = left;

            if (right < heapSize && input[right] > input[largest])
                largest = right;

            if (largest != index)//有更動
            {
                swap(input, index, largest);
                MaxHeapify(input, heapSize, largest);//往下繼續做比對
            }
        }
    }
}
