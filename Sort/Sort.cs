﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    public abstract class Sort : ISort
    {
        protected int[] query;

        public void Show()
        {
            Console.WriteLine(this);
            //開始計算Sort執行時間
            DateTime startTime = DateTime.Now;
            this.Sorting();
            //計算Sort執行時間
            long comTime = DateTime.Now.Ticks - startTime.Ticks;
            Console.WriteLine("Sort總共費時{0}, {1}毫秒 耗時{2}秒", comTime, comTime / 10000, comTime / 10000000);


            foreach (int item in this.query)
            {
                Console.Write("{0} ", item);
            }
            Console.WriteLine("");
            Console.WriteLine("");
        }

        abstract public void Sorting();

        public void SetRandomQuery(int queryLength, int range)
        {
            this.query = GetRadomQuery(queryLength, range);
        }

        private int[] GetRadomQuery(int number, int range)
        {
            Random rnd = new Random();
            int[] result = new int[number];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = rnd.Next(range);
            }

            return result;

        }

        protected void swap(int[] data, int i, int j)
        {
            var tmp = data[i];
            data[i] = data[j];
            data[j] = tmp;
            //Console.Write(this.count);
            //for (int index = 0; index < this.query.Length; index++)
            //{
            //    if (index == i || index == j)
            //        Console.Write(" -{0}-", this.query[index]);
            //    else
            //        Console.Write(" {0}", this.query[index]);
            //}


            //Console.WriteLine("");
        }
    }
}
